## IDC
The IDC procedure infers the cooperativity from a voltage-clamp current recording. The following notebook demonstrates how to apply IDC in practice at the example of simulated data.


## Installation
Install the package MUSCLE.

```
install.packages("MUSCLE_0.1.0.tar.gz", repos = NULL, type ="source")
```

We load the necessary packages and functions for the IDC procedure:

```
library(MUSCLE)
source("IDC.r")
```

## Usage
First we simulate a data set with VND parameter $\theta=(\lambda_0,\lambda_1,\lambda_2,\eta_1,\eta_2,\eta_3)=(0.8,0.5,0.3,0.3,0.2,0.9 )$, which corresponds to a positively cooperative Markov chain.

```
# L=3 ion channels
L=3
n=10000
#Fit VND-model
theta <- c(0.8,0.5,0.3,0.2,0.9)
model <- transition_matrix(theta)
# Specify initial distribution
pi <- c(rep(1/(L+1),L+1))
# Simulate data
S=c(simulate_MC(n,model,pi))
# We add some noise to the data
Y=S+rnorm(n,0,0.1)
```
We idealise the data, i.e. we recover the underlying piecewise constant function.
```
# Calculate the idealisation by MUSCLE
yseg1=MUSCLE(Y,q=simulQuantile_MUSCLE(length(Y),alpha=0.94),dyadic = TRUE,split = TRUE)
# Fit the estimated piecewise constant function
yhat1=rep(1,n)
for (j in 2:length(yseg1$value)) {
    yhat1[(yseg1$left[j-1]:yseg1$left[j])] = yseg1$value[j-1]
}
yhat1[(yseg1$left[length(yseg1$value)]):yseg1$n]=yseg1$value[length(yseg1$value)]
```
We discretise the data into the levels $\{0,..,L\}$.
```
l1 = 3
v1 = seq(0,1,1/l1)
ycls1 = kmeans(yhat1,  min(yhat1)+v1*(max(yhat1)-min(yhat1)))
ydis1 = ycls1$cluster-1
```
We estimate the underlying VND paramter by a minimum distance estimator.
```
#Calculate the estimated transition matrix
Q_hat=Q_hat_f(ydis1)
#find appropriate initial values for the optimisation algorithm
theta_init=Grid_search_R(max(ydis1),Q_hat,LossRcpp)
#Calculate the minimum ditance estimator
MDE=minimum_distance_est(ydis1,theta_init=theta_init)
```
If the cooperative ratios are greater than 1, the model is considered to behave positively cooperative.
```
ratios_cooperative = c(MDE[1]/MDE[2:3],MDE[6]/MDE[4:5])
print(ratios_cooperative)
```

