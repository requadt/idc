# This file contains all necessary functions, which are not importated from a package available in CRAN





# The objective function, the minimum distance estimator minimizes; it is the squared Frobenius norm of the difference of Q(h) and Q_hat_0,
# where Q(h) is the transition matrix of a VND-MC with VND parameter h
# If L is the number of channels, h  needs to be a vector in [0,1]^{2L}
Rcpp::cppFunction("
double LossRcpp(NumericVector h, NumericMatrix Q_hat_0) {
    int L = h.size() / 2;
    double loss = 0.0;
    
    for (int i = 0; i <= L; ++i) {
        for (int j = 0; j <= L; ++j) {
            double p = 0.0;
            
            for (int r = std::max(0, i - j); r <= std::min(i, L - j); ++r) {
                double lambda = (i == L) ? 1.0 : h[i];
                double eta = (i == 0) ? 1.0 : h[i + L-1];
                
                double binom_i_r = R::choose(i, r);
                double binom_L_i_j_i_r = R::choose(L - i, j - i + r);
                
                p += binom_i_r * binom_L_i_j_i_r *
                     std::pow(eta, i - r) * std::pow(1 - eta, r) *
                     std::pow(lambda, L - j - r) * std::pow(1 - lambda, j - i + r);
            }
            
            loss += std::pow(p - Q_hat_0(i, j), 2);
        }
    }
    
    return loss;
}")



# For a given realisation of a VND-MC S
# L : the number of ion channels
# theta_init : the initial value of the optimisation algorithm; needs to be a vector in [0,1]^{2L}
minimum_distance_est<-function(S,L=max(S),theta_init){
  n=length(S)
  #calculate empirical estimator
  Q_hat <- matrix(0, nrow=L+1,ncol=L+1)
  for(i in 0:L){
    for(j in 0:L){
      count1 <- 0
      count2 <- 0
      for(k in 1:n){
        if(k>1){
          if(S[k]==j&S[k-1]==i) {
            count1 <- count1 +1
          }     
        }
        if(S[k]==i){
          count2 <- count2 +1
        }
      } 
      Q_hat[i+1,j+1]=(n/(n-1))*count1/count2
    }
  }
  
  #prepare the optimisation constraints
  ui=matrix(0,nrow=4*L,ncol=2*L)
  ci=rep(0,2*L)
  for(j in 1:(2*L)){
    ui[j,j]=1
    ui[2*L+j,j]=-1
    ci[2*L+j]=-1
  }
  #calculate the minimum distance estimator
  A=constrOptim(theta_init,f=function(u){return(LossRcpp(u,Q_hat))},ui=ui,ci=ci,grad=NULL)$par
  return(A)
}


#calculate the transition matrix of a VND-MC with VND parameter h
# Q[i,j]=q_{i,j}=P(S_{2}=j|S_1=i)

transition_matrix=function(h){
  L <- length(h)/2
  Q <- matrix(0,nrow=L+1,ncol=L+1)
  for(i in 0:L){
    for(j in 0:L){
      p<-0
      for(r in max(0,i-j):min(i,L-j)){
        lambda=ifelse(i==L,1,h[i+1])
        eta=ifelse(i==0,1,h[i+L])
        p=p+choose(i,r)*choose(L-i,j-i+r)*eta^(i-r)*(1-eta)^(r)*lambda^(L-j-r)*(1-lambda)^(j-i+r)
      }
      Q[i+1,j+1] <- p
    }
  }
  return(Q)
}




#simulate a Markov Chain from a transition matrix
#the following code is due to [Vanegas et al, 2024]


Rcpp::cppFunction("
NumericVector simulate_MC(int n_data, NumericMatrix P_trans, NumericVector initial) {
  int n_levels = P_trans.nrow();
  Rcpp::NumericVector markov_chain(n_data);
  Rcpp::NumericVector randoms = Rcpp::runif(n_data);
  markov_chain[0] = 0;
  double tmp = 0.;
  for (int j = 0; j < n_levels; j++) {
    tmp += initial[j];
    if (randoms[0] < tmp) {
      markov_chain[0] = j;
      break;
    }
  }
  for (int t=1; t<n_data; t++){
    int i = markov_chain[t-1];
    tmp = 0.;
    markov_chain[t] = 0;
    for (int j = 0; j < n_levels; j++) {
      tmp += P_trans(i,j);
      if (randoms[t] < tmp) {
        markov_chain[t] = j;
        break;
      }
    }
  }
  
  return markov_chain;
}")


#Grid-search to find initial values
Grid_search_R <- function(L,Q_hat,LossRcpp,stepsize=0.1){
  #L integer, Q_hat is a L+1 times L+1 matrix
  #LossRcpp is some R function
  l <- rep(list(seq(0.1,1,stepsize)) ,2*L)
  GRID <- expand.grid(l)
  Nrow_ <- dim(GRID)[1]
  losses <- rep(0,Nrow_)
  for(j in 1:Nrow_){
    losses[j] <- LossRcpp(GRID[j,],Q_hat_0 = Q_hat)
  }
  M <- which.min(losses)
  return(GRID[M,])
}

#Calculation of the empirical transition matrix for an observed Markov chain S
Q_hat_f <- function(S,L=max(S)){
  n <- length(S)
  Q_hat <- matrix(0, nrow=L+1,ncol=L+1)
  for(i in 0:L){
    for(j in 0:L){
      count1 <- 0
      count2 <- 0
      for(k in 1:n){
        if(k>1){
          if(S[k]==j&S[k-1]==i) {
            count1 <- count1 +1
          }     
        }
        if(S[k]==i){
          count2 <- count2 +1
        }
      } 
      Q_hat[i+1,j+1]=count1/count2
    }
  }
  return(Q_hat)
}


#solves a k-means clustering problem under the constraint that the clustering centers are equidistant

constrained_k_means=function(x,k){
  if( k<=1){
    print("infeasable number of clusters")
    break()
  }
  y=sort(x,index.return=TRUE)
  index= y$ix
  y=y$x
  n=length(y)
  min_=min(y)
  max_=max(y)
  
  clustering_para=c(NA,NA)
  clustering_center=rep(NA,k)
  objective_value_optimal=NA
  
  #change_points=matrix(NA,nrow=n*k,ncol=k-1)
  optimal_changepoint=matrix(NA,nrow=n,ncol=k)
  C=function(j,i,y,mu,c,L){
    if(i<j){
      print("Error wrong input")
      return(NA)
    }
    y_sub=y[j:i]
    C_=sum((y_sub-mu-c(L-1))^2)
    return(C_)
  }
  update_coeff=function(old_coeff,T_previous,T_new,j,y){
    #T_previous, T_now  change points
    # Calculate the coefficients of f_(T_new,m)
    #s=T_[j]-T_[j-1]
    s=T_new -T_previous
    #d=y[(T_[j-1]+1):T_[j]]
    d=y[(1+T_previous):(T_new)]
    u = old_coeff+c((j-1)^2*s,s,2*(j-1)*s,-2*(j-1)*sum(d),-2*sum(d),sum(d^2) )
    return(u)
  }
  f_function=function(coeff){
    if(length(u)!=6 ){
      print("wrong number of coefficients")
      return(NA)
    }
    f=function(h){
      c=h[1]
      u=h[2]
      return(coeff[1]*c^2+coeff[2]*u^2+coeff[3]*c*u+coeff[4]*c+coeff[5]*u+coeff[6])
    }
    return(f)
  }
  find_optimum=function(h,c_0,u_0){
    #explicitly find minimum of quadratic loss function
    c_hat=NA
    mu_hat=NA
    if((4*h[1]*h[2]-h[3]^2)>0){
      c_hat=(-2*h[2]*h[4]+h[5]*h[3])/(4*h[1]*h[2]-h[3]^2)
      mu_hat=(-2*h[1]*h[5]+h[4]*h[3])/(4*h[1]*h[2]-h[3]^2)
    }
    if((h[1]==h[3])&(h[3]==h[4])&(h[4]==0)){
      c_hat=c_0
      mu_hat=-h[5]/(2*h[2])
    }
    if((h[2]==h[3])&(h[3]==h[5])&(h[5]==0)){
      c_hat=-h[4]/(2*h[1])
      mu_hat=u_0
    }
    objective_=h[1]*c_hat^2+h[2]*mu_hat^2+h[3]*c_hat*mu_hat+h[4]*c_hat+h[5]*mu_hat+h[6]
    
    res=list("par"=c(c_hat,mu_hat),"value"=objective_)
    return(res)
  }
  #a row f_coeff[i,,m], i.e. the vector (a_1,..,a_6) corresponds to the function
  # (mu,c) -> a_1c^2 + a_2mu^2 + a_3 c mu+ a_4 c + a_5 mu + a_6
  #  f_coeff[i+1,,m+1] correspond to the coefficients of f(i,m)
  f_coeff=array(NA,dim=c(n+1,6,k+1) )
  #f^0==0
  f_coeff[,,1]=c(rep(0,6))
  f_coeff[1,,]=rep(0,6)
  function_values=rep(NA,n)
  counter = 1
  for(m in 1:k){
    #calculate f(m,m),..f(n-k+m,m)
    #those are used to calculate f(m+1,m+1),..,f(n-k+m+1,m+1)
    for(j in m:(n-k+m)){
      #calculate f(j,m)
      #print(j)
      if(m>1){
        Number_row=j-1-m+2
        candidates_coefficients = matrix(NA,ncol=6,nrow=Number_row)
        #candidates_position[s-m+2,] corresponds to optimal values (c_s,\hat{mu}_s) of the function f(s,m-1)+C(s+1,j,m)
        candidates_position = matrix(NA,ncol=2,nrow=Number_row)
        objective_values = rep(NA,Number_row)
        for(s in (m-1):(j-1) ){
          #coefficients corresponding to f(s,m-1)
          old_coeff=f_coeff[s+1, ,m]
          #coefficients corresponding to f(s,m-1)+C(s+1,j,m)
          coeff=update_coeff(old_coeff,T_previous = s,T_new =j ,m,y)
          candidates_coefficients[s-m+2,]=coeff
          
          optim=find_optimum(coeff,1,mean(y))
          #pos_cand = argmin_{c,mu} f_cand(c,mu)
          pos_cand=optim$par
          candidates_position[s-m+2,]= pos_cand
          #optim$value=f_cand(pos_cand), i.e. f_cand evaluated at the optimal (c,mu)
          objective_values[s-m+2]=optim$value
          
        }
        #safe where the minimum happens as optimal_changepoint[j,m]
        #T_{m-1}
        optimal_changepoint[j,m]=which.min(objective_values)+m-2
        a=optimal_changepoint[j,m]-m+2
        #choose f(j,m) as the minimum and safe the corresponding coefficients
        f_coeff[j+1,,m+1] = candidates_coefficients[a,]
        if( (m==k)&(j==n)){
          clustering_para=candidates_position[a,]
          objective_value_optimal=objective_values[a]
        }
        
        
      }
      
      if(m==1){
        #calc f(j,1)=C(0+1,j,1)
        Number_row=1
        candidates_coefficients = matrix(NA,ncol=6,nrow=Number_row)
        #candidates_position[s-m+2,] corresponds to optimal values (c_s,\hat{mu}_s) of the function f(s,m-1)+C(s+1,j,m)
        candidates_position = matrix(NA,ncol=2,nrow=Number_row)
        objective_values = rep(NA,Number_row)
        coeff=update_coeff(rep(0,6),T_previous = 0,T_new =j ,1,y)
        candidates_coefficients[1,]=coeff
        
        
    
        optim=find_optimum(coeff,1,mean(y))
        #pos_cand = argmin_{c,mu} f_cand(c,mu)
        pos_cand=optim$par
        candidates_position[1,]= pos_cand
        #optim$value=f_cand(pos_cand), i.e. f_cand evaluated at the optimal (c,mu)
        objective_values[1]=optim$value
        
        #safe where the minimum happens as optimal_changepoint[j,m]
        #T_{m-1}
        optimal_changepoint[j,1]=which.min(objective_values)-1
        #choose f(j,m) as the minimum and safe the corresponding coefficients
        f_coeff[j+1,,2] = coeff
      }
      
    }
  }
  #find optimal change points by backtracing
  change_points_global = c(rep(NA,k-1),n)
  #for( d in 1:(k-1)){
  for(d in seq(1,k-1,1)){
    T_=change_points_global[k-d+1]
    change_points_global[k-d]=optimal_changepoint[T_,k-d+1]
  }
  clustering=y
  clustering[1:change_points_global[1]]=1
  for(i in 1:(k-1)){
    clustering[(1+change_points_global[i]):change_points_global[i+1]]=i+1
  }
  mu_=clustering_para[2]
  c_=clustering_para[1]
  clustering_center=mu_+c_*seq(0,k-1,1)
  output=list("change_points"=change_points_global[1:(k-1)],"clustering"=clustering[order(index)],"(c,mu)"=clustering_para,"clustering_centers"=clustering_center,"objective_value"=objective_value_optimal)
  return(output)
}
